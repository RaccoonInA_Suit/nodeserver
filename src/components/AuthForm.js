import React, {useState} from "react";
import { Button, Form } from 'react-bootstrap';
import axios from "axios";



const AuthForm = () => {

    const [result, setResult] = useState(null);

    const [state, setState] = useState({
        email: '',
        password: ''
      });
    
      const sendAuth = event => {
        event.preventDefault();
        axios
        .post('/send', { ...state })
        .then(response => {
          setResult(response.data);
          setState({ email: '', password: ''});
        })
        .catch(() => {
          setResult({ success: false, message: 'Something went wrong. Try again later'});
      });
      };
    
      const onInputChange = event => {
        const { name, value } = event.target;
    
        setState({
          ...state,
          [name]: value
        });
      };



    return (
        <>
            <Form onSubmit={sendAuth}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control onChange={onInputChange} type="text" name="email" placeholder="Enter email" value={state.email} />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control onChange={onInputChange} type="password" name="password" placeholder="Password" value={state.password} />
                </Form.Group>

                <Button variant="primary" type="submit">
                Submit
                </Button>
            </Form>  
        </>

    )
}

export default AuthForm;